﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SitemapGraber
{
    interface IAlgorithme<T>
    {
        bool Contains(T data);
        Node<T> GetFirstValue();
    }

    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }
        public T Data { get; set; }
        public Node<T> Next { get; set; }
    }
    public class СircularList<T> : IEnumerable<T>,  IAlgorithme<T>
    {
        Node<T> head; 
        Node<T> tail;
        int count;  
        
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            
            if (head == null)
            {
                head = node;
                tail = node;
                tail.Next = head;
            }
            else
            {
                node.Next = head;
                tail.Next = node;
                tail = node;
            }
            count++;
        }
        public bool Remove(T data)
        {
             
            Node<T> current = head;
            Node<T> previous = null;
 
            if (this.IsEmpty()) return false;
 
            do
            {
                if (current.Data.Equals(data))
                {
                   
                    if (previous != null)
                    {
                        // убираем узел current, теперь previous ссылается не на current, а на current.Next
                        previous.Next = current.Next;
 
                        // Если узел последний,
                        // изменяем переменную tail
                        if (current == tail)
                            tail = previous;
                    }
                    else // если удаляется первый элемент
                    {
                         
                        // если в списке всего один элемент
                        if(count==1)
                        {
                            head = tail = null;
                        }
                        else
                        {
                            head = current.Next;
                            tail.Next = current.Next;
                        }
                    }
                    count--;
                    return true;
                }
 
                previous = current;
                current = current.Next;
            } while (current != head);
 
            return false;
        }
 
        public int Count { get { return count; } }

        public bool IsEmpty()
        {
            return count == 0; 
        }
 
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }
 
         Node<T> IAlgorithme<T>.GetFirstValue() 
         { 
             return head; 
         }
        
        bool IAlgorithme<T>.Contains(T data)
        {
         
            Node<T> current = head;
            if (current == null) return false;
            do
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != head);
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
 
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }
    }
}